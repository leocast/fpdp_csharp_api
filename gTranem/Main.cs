﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApiCSharp;
using SocketSystem;

namespace gTranem
{
    public partial class Main : MetroFramework.Forms.MetroForm
    {
        private static Main _mainMenu;

        public Main()
        {
            InitializeComponent();
        }

        public static Main MainMenu
        {
            get
            {
                if (_mainMenu == null)
                {
                    _mainMenu = new Main();
                }
                return _mainMenu;
            }
        }

        private void Main_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnUsuarios_Click(object sender, EventArgs e)
        {
            Login lg = new Login();
            lg.Show();
            this.Hide();

        }

        private void btnHuella_Click(object sender, EventArgs e)
        {
            Controller cl = new Controller();
            cl.Show();
            this.Hide();
        }
    }
}
