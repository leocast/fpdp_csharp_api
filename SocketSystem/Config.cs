﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SocketSystem
{
    public partial class Config : Form
    {
        Ini config;
        public Config(Ini _config)
        {
            InitializeComponent();
            config = _config;

            txtServer.Text = config.Read("server");

            if (config.Read("ip") == "local")
                rbLocal.Checked = true;
            else
                rbInternet.Checked = true;
        }

        private void btnAceptar_Click(object sender, EventArgs e)
        {

            config.Write("server", txtServer.Text);

            if (rbLocal.Checked)
                config.Write("ip", "local");
            else
                config.Write("ip", "internet");

            MessageBox.Show("Los cambios han sido guardados, se reiniciará la aplicación para que estos surjan efecto");

            Application.Restart();
            Environment.Exit(0);
        }
    }
}
