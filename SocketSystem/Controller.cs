﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApiCSharp;

namespace SocketSystem
{

    public partial class Controller : Form
    {
        SocketController socket;
        string IP;
        string serverIp;
        Ini config;

        string ipType;

        public Controller()
        {
            InitializeComponent();

            //Inicializamos la configuración
            config = new Ini("config/config.ini");

            //TEST FUERA DEL LOCAL: geniuscode.ddns.net:81/gtranem
            serverIp = config.Read("server");

            if (config.Read("ip") == "local")
                ipType = "local";
            else
                ipType = "internet";

      
            lblIp.Text = serverIp;

            socket = new SocketController(serverIp);

            if(ipType == "local")
                IP = socket.getIP();
            else
                IP = socket.getPublicIP();
            
            lblIPPublica.Text = $" {IP}";

            listenLlamarHuella();

            notifyIcon1.Icon = SystemIcons.Application;
        }

        private void listenLlamarHuella()
        {
            socket.Listen(this, $"{IP}_llamarHuella", getDatos());
        }

        private Action<object> getDatos() {

            return (object data) =>
                {
                    dynamic huellas = data;

                    string text = $">> [Solicitud]: ({huellas.codigo}) {huellas.nombre} - {DateTime.Now}";

                    txtActivity.Text += text +  Environment.NewLine;

                    socket.makeLog(text);

                    DPFP.Template template1 = FP_Helper.Base64ToTemplate(huellas.huella01.ToString());
                    DPFP.Template template2 = FP_Helper.Base64ToTemplate(huellas.huella02.ToString());

                    FormValidator validator = new FormValidator(socket, template1, template2, ipType);
                    validator.TopMost = true;
                    validator.Show();
                    validator.Focus();
                };
        }

        private void Controller_Resize(object sender, EventArgs e)
        {
            bool cursorNotInBar = Screen.GetWorkingArea(this).Contains(Cursor.Position);

            if (this.WindowState == FormWindowState.Minimized && cursorNotInBar)
            {
                this.ShowInTaskbar = false;
                notifyIcon1.Visible = true;
                this.Hide();
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Show();
            WindowState = FormWindowState.Normal;
            notifyIcon1.Visible = false;
        }

        private void btnConfig_Click(object sender, EventArgs e)
        {

            Config configForm = new Config(config);

            configForm.Show();

        }

        private void Controller_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Restart();
            Environment.Exit(0);
        }
    }

}
