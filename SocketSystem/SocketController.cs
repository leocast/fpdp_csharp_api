﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quobject.SocketIoClientDotNet.Client;
using ApiCSharp;
using System.Windows.Forms;
using System.Net;
using System.Web;
using System.Text.RegularExpressions;

namespace SocketSystem
{
    public class SocketController
    {
        public Quobject.SocketIoClientDotNet.Client.Socket socket;

        public SocketController(string serverIP)
        {
            socket = IO.Socket(serverIP);
        }

        public void Disconnect()
        {
            socket.Disconnect();
        }

        public void Emit(string emitTo, string data)
        {
            socket.Emit(emitTo, data);
        }

        public void Listen(Form parent, string _event, Action<object> _action)
        {
            
socket.On(_event, (data) =>
            {
                FP_Helper.InvokeObject(parent, _action, data);

            });
        }

        public string getIP()
        {
            string hostName = Dns.GetHostName();

            return Dns.GetHostByName(hostName).AddressList[0].ToString();

        }

        public string getPublicIP() {
            string externalIP = "";
            externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
            externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}")).Matches(externalIP)[0].ToString();
            return externalIP;
        }

        public void makeLog(string text) {
            using (System.IO.StreamWriter file =
            new System.IO.StreamWriter("logs/Log.txt", true))
            {
                file.WriteLine(text);
            }
        }
    }
}
