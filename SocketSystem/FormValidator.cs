﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ApiCSharp;
using System.Dynamic;
using Newtonsoft.Json;

namespace SocketSystem
{
    public partial class FormValidator : MetroFramework.Forms.MetroForm, DPFP.Capture.EventHandler
    {
        FP_Controller FP;
        DPFP.Template template1;
        DPFP.Template template2;
        Verification verification;
        SocketController socket;
        string ipType;

        public FormValidator(SocketController _socket, DPFP.Template _template1, DPFP.Template _template2, string _ipType)
        {
            InitializeComponent();
            FP = new FP_Controller(this);

            this.socket = _socket;
            this.template1 = _template1;
            this.template2 = _template2;
            this.ipType = _ipType;
        }

        #region Eventos:
        public void OnComplete(object Capture, string ReaderSerialNumber, DPFP.Sample Sample)
        {
            verification = new Verification(Sample);

            bool result = verification.checkTwoTemplates(template1, template2);

            FP.Stop();

            dynamic response = new ExpandoObject();
            response.result = result;

            if (result)
            {
                response.mensaje = "La huella coincide";
                socket.makeLog(">> [Estatus] Huella correcta" + Environment.NewLine);
            }
            else {
                response.mensaje = "La huella no coincide";
                socket.makeLog(">> [Estatus] Huella incorrecta" + Environment.NewLine);
            }

            string IP = "";

            if (ipType == "local")
                IP = socket.getIP();
            else
                IP = socket.getPublicIP();

            string json = JsonConvert.SerializeObject(response);

            socket.Emit($"{IP}_mandarHuellaServer", json);

            Action action = () =>
            {
                this.Hide();
            };

            FP_Helper.Invoke(this, action);

        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {
        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {
        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {
        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {
        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {
        }
        #endregion

    }
}
