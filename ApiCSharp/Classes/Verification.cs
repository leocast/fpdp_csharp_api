﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Library;

namespace ApiCSharp
{
    public class Verification
    {
        private DPFP.Template Template;
        private DPFP.Sample Sample;
        private DPFP.Verification.Verification Verificator;

        //Constructor
        public Verification(DPFP.Sample _sample) {
            this.Sample = _sample;
            Verificator = new DPFP.Verification.Verification();
        }

        public async Task<bool> check() {
            testController TC = new testController();

            this.Template = await TC.getTemplate();

            return matchSample();
        }

        public bool checkTwoTemplates(DPFP.Template _template1, DPFP.Template _template2)
        {
            this.Template = _template1;

            bool result =  matchSample();

            if (result)
                return result;

            this.Template = _template2;

            result = matchSample();

            return result;
        }

        public bool matchSample() {
            //Procesa el sample(huella) y crea un feature set para la verificación
            DPFP.FeatureSet features = ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Verification);

            if (features != null)
            {
                //Compara el feature con el template de la db
                DPFP.Verification.Verification.Result result = new DPFP.Verification.Verification.Result();
                Verificator.Verify(features, Template, ref result);

                return result.Verified;
            }
            else {
                return false;
            }
        }

        protected DPFP.FeatureSet ExtractFeatures(DPFP.Sample Sample, DPFP.Processing.DataPurpose Purpose)
        {
            DPFP.Processing.FeatureExtraction Extractor = new DPFP.Processing.FeatureExtraction();  // Create a feature extractor
            DPFP.Capture.CaptureFeedback feedback = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet features = new DPFP.FeatureSet();
            Extractor.CreateFeatureSet(Sample, Purpose, ref feedback, ref features);            // TODO: return features as a result?
            if (feedback == DPFP.Capture.CaptureFeedback.Good)
                return features;
            else
                return null;
        }
    }
}
