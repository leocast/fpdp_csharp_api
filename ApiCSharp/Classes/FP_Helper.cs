﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using System.IO;

namespace ApiCSharp
{
    public static class FP_Helper
    {
        //Convertir Sample a bitmap
        public static Bitmap ConvertSampleToBitmap(DPFP.Sample Sample)
        {
            DPFP.Capture.SampleConversion Convertor = new DPFP.Capture.SampleConversion();  // Create a sample convertor.
            Bitmap bitmap = null;                                                           // TODO: the size doesn't matter
            Convertor.ConvertToPicture(Sample, ref bitmap);                                 // TODO: return bitmap as a result
            return bitmap;
        }

        //Extrae los features del sample
        public static DPFP.FeatureSet ExtractFeatures(DPFP.Sample Sample, DPFP.Processing.DataPurpose Purpose)
        {
            DPFP.Processing.FeatureExtraction Extractor = new DPFP.Processing.FeatureExtraction();  // Create a feature extractor
            DPFP.Capture.CaptureFeedback feedback = DPFP.Capture.CaptureFeedback.None;
            DPFP.FeatureSet features = new DPFP.FeatureSet();
            Extractor.CreateFeatureSet(Sample, Purpose, ref feedback, ref features);            // TODO: return features as a result?
            if (feedback == DPFP.Capture.CaptureFeedback.Good)
                return features;
            else
                return null;
        }

        //Llamar action
        public static void Invoke(Form calledFrom, Action _action)
        {
            while (!calledFrom.IsHandleCreated)
                System.Threading.Thread.Sleep(100);

            calledFrom.Invoke((MethodInvoker)delegate {
                _action();
            });
        }

        //Llamar action
        public static void InvokeObject(Form calledFrom, Action<object> _action, object data)
        {
            while (!calledFrom.IsHandleCreated)
                System.Threading.Thread.Sleep(100);

            calledFrom.Invoke((MethodInvoker)delegate {
                _action(data);
            });
        }

        //Procesar Huella
        public static void Process(Form calledFrom, DPFP.Sample Sample, DPFP.Processing.Enrollment Enroller, 
            Action _onReady, Action _onFailed, Action _updateStatus = null, Action _extraAction = null)

        {
            if (_extraAction != null)
                FP_Helper.Invoke(calledFrom, _extraAction);

            // Procesa el sample y crea el feature set.
            DPFP.FeatureSet features = FP_Helper.ExtractFeatures(Sample, DPFP.Processing.DataPurpose.Enrollment);

            // Verifica la calidad del sample y la añade al enroller si es buena
            if (features != null) try
                {
                    Enroller.AddFeatures(features);     // Agrega el feature al template.
                }
                finally
                {
                    if (_updateStatus != null)
                        FP_Helper.Invoke(calledFrom, _updateStatus);

                    // Verifica si se termino de crear la plantilla.
                    switch (Enroller.TemplateStatus)
                    {
                        case DPFP.Processing.Enrollment.Status.Ready:   
                            FP_Helper.Invoke(calledFrom, _onReady);
                            break;

                        case DPFP.Processing.Enrollment.Status.Failed:  
                            FP_Helper.Invoke(calledFrom, _onFailed);
                            break;
                    }
                }
        }

        //Template to Base64 string
        public static string TemplateToBase64(DPFP.Processing.Enrollment Enroller) {

            byte[] fingerPrint = Enroller.Template.Bytes;

            return Convert.ToBase64String(fingerPrint);
        }

        //Base64 to Template
        public static DPFP.Template Base64ToTemplate(string base64) {
            byte[] templateBytes = Convert.FromBase64String(base64); //Se transforma la Base64 a bytes[]

            MemoryStream ms = new MemoryStream(templateBytes); //Se encapsulan los datos en la memoria

            DPFP.Template template = new DPFP.Template(ms); //Se crea un fp template con los datos del stream

            return template;
        }
    }
}
