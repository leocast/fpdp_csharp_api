﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApiCSharp
{
    public class FP_Controller
    {
        private DPFP.Capture.Capture Capturer;
        private DPFP.Capture.EventHandler eventHandler;

        //Constructor
        public FP_Controller(DPFP.Capture.EventHandler _eventHandler) {
            this.eventHandler = _eventHandler;
            this.Init();
            this.Start();
        }

        public void Init()
        {
            try
            {
                Capturer = new DPFP.Capture.Capture();				// Create a capture operation.

                if (null != Capturer)
                    Capturer.EventHandler = eventHandler;					// Subscribe for capturing events.
                else
                    MessageBox.Show("No se puede inicializar la operación");
            }
            catch
            {
                MessageBox.Show("Can't initiate capture operation!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        public void Start()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StartCapture();
                    //MessageBox.Show("Capturador iniciado");
                }
                catch
                {
                    MessageBox.Show("Error al iniciar el capturador");
                }
            }
        }

        public void Stop()
        {
            if (null != Capturer)
            {
                try
                {
                    Capturer.StopCapture();
                }
                catch
                {
                    MessageBox.Show("No se puede cerrar el capturador");
                }
            }
        }
    }
}
