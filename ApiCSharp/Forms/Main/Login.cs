﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Http;
using Library;
using MetroFramework;

namespace ApiCSharp
{
    public partial class Login : MetroFramework.Forms.MetroForm
    {
        Library.testController TC;

        public Login()
        {
            InitializeComponent();
       
        }

        #region Funcionalidad del form
        private void Login_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void btnAtras_Click(object sender, EventArgs e)
        {
            Application.Restart();
            Environment.Exit(0);
        }

        private void txtPass_Enter(object sender, EventArgs e)
        {
            txtPass.isPassword = true;
        }

        #endregion

        private async void btnAceptar_Click(object sender, EventArgs e)
        {
            if (this.validateEmpty())
            {
                startLoader();

                TC = new testController();

                dynamic usuario = await TC.login(txtUser.Text, txtPass.Text);

                if (usuario == null)
                {
                    stopLoader();
                    MetroMessageBox.Show(this, Environment.NewLine + "El usuario y/o contraseña son incorrectos ", "Usuario y/o contraseña incorrecta",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);

                    txtPass.Text = "";
                    txtUser.Focus();
                }
                else
                {
                    Session.usuario = usuario;
                    FormGrid frm = new FormGrid();
                    frm.Show();
                    this.Hide();
                    stopLoader();
                }
            }
        }

        private bool validateEmpty() {
            if (txtUser.Text == "" || txtPass.Text == "")
            {
                MetroMessageBox.Show(this, Environment.NewLine + "El usuario y/o contraseña estan vacíos ", "Campos vacíos",
                    MessageBoxButtons.OK, MessageBoxIcon.Warning);

                return false;
            }

            return true;
        }

        private void Login_Load(object sender, EventArgs e)
        {
            this.Size = new Size(494, 397);
            loader.Location = new Point(531, 13);
        }

        private void startLoader() {
            loader.Location = new Point(12, 13);
        }

        private void stopLoader() {
            loader.Location = new Point(531, 13);
        }

        private void txtPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtUser.Text != "" && txtPass.Text != "")
                if (e.KeyChar == 13)
                    btnAceptar_Click(new object(), new EventArgs());
        }

        private void txtUser_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (txtPass.Text != "" && txtUser.Text != "")
                if (e.KeyChar == 13)
                    btnAceptar_Click(new object(), new EventArgs());
        }
    }
}
