﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ApiCSharp
{
    public partial class EnrollmentApp : MetroFramework.Forms.MetroForm, DPFP.Capture.EventHandler
    {
        public delegate void OnTemplateEventHandler(DPFP.Template template); //Manejador del template

        public event OnTemplateEventHandler OnTemplate;  //Evento del template

        FP_Controller FP; 

        private DPFP.Processing.Enrollment Enroller;

        Registrar registrarForm;

        dynamic dedo;

        public EnrollmentApp(Registrar _registrarForm, dynamic _dedo)
        {
            InitializeComponent();
            FP = new FP_Controller(this);

            Enroller = new DPFP.Processing.Enrollment();
            registrarForm = _registrarForm;
            dedo = _dedo;
        }
      
        protected void Process(DPFP.Sample Sample)
        {

            Action onReady = () =>
            {
                dedo.template64 = FP_Helper.TemplateToBase64(Enroller);
                dedo.registrar = true;

                if (dedo.dedo == 1)
                    registrarForm.template.dedo1 = dedo;
                else 
                    registrarForm.template.dedo2 = dedo;

                registrarForm.setImagenHuella(FP_Helper.ConvertSampleToBitmap(Sample), dedo.dedo);

                lblPermiso.ForeColor = Color.ForestGreen;
                lblPermiso.Text = "Listo, puede cerrar esta ventana";
                FP.Stop();
                this.Close();
            };

            Action onFailed = () =>
            {
                Enroller.Clear();
                FP.Stop();
                ActualizarStatus();
                OnTemplate(null);
                FP.Start();
            };

            Action updateStatus = () => {
                this.ActualizarStatus();
            };

            Action dibujarHuella = () =>
            {
                this.DibujarHuella(Sample);
            };

            FP_Helper.Process(this, Sample, Enroller, onReady, onFailed, updateStatus, dibujarHuella);
        }

        protected void ActualizarStatus()
        {
            Action update = () =>
            {
                lblStatus.Text = String.Format("Muestras necesarias: {0}", Enroller.FeaturesNeeded);
            };

            FP_Helper.Invoke(this, update);
        }

        protected void Log(string message)
        {
            Action log = () =>
            {
                txtLog.AppendText(">> " + message + "\r\n");
            };

            FP_Helper.Invoke(this, log);
        }

        protected void DibujarHuella(DPFP.Sample Sample)
        {
            Bitmap huella = FP_Helper.ConvertSampleToBitmap(Sample);

            pbxHuella.Image = new Bitmap(huella, pbxHuella.Size);
        }

        #region Events:
        public void OnComplete(object Capture, string ReaderSerialNumber, DPFP.Sample Sample)
        {
            this.Process(Sample);
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber) { }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber) { }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber) { }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber) {

            MessageBox.Show("El lector no está conectado");

            Action closeForm = () =>
            {
                this.Close();
            };

            FP_Helper.Invoke(this, closeForm);
        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback) { }

        #endregion

        private void EnrollmentApp_FormClosed(object sender, FormClosedEventArgs e)
        {
            FP.Stop();
        }
    }
}