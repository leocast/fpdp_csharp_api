﻿namespace ApiCSharp
{
    partial class SelectDedo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectDedo));
            this.dedo1 = new System.Windows.Forms.PictureBox();
            this.dedo2 = new System.Windows.Forms.PictureBox();
            this.dedo3 = new System.Windows.Forms.PictureBox();
            this.dedo4 = new System.Windows.Forms.PictureBox();
            this.dedo5 = new System.Windows.Forms.PictureBox();
            this.palma1 = new System.Windows.Forms.PictureBox();
            this.bunifuCustomLabel1 = new ns1.BunifuCustomLabel();
            this.dedo10 = new System.Windows.Forms.PictureBox();
            this.dedo9 = new System.Windows.Forms.PictureBox();
            this.dedo6 = new System.Windows.Forms.PictureBox();
            this.dedo8 = new System.Windows.Forms.PictureBox();
            this.dedo7 = new System.Windows.Forms.PictureBox();
            this.palma2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.dedo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palma1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.palma2)).BeginInit();
            this.SuspendLayout();
            // 
            // dedo1
            // 
            this.dedo1.BackColor = System.Drawing.Color.Transparent;
            this.dedo1.Image = ((System.Drawing.Image)(resources.GetObject("dedo1.Image")));
            this.dedo1.Location = new System.Drawing.Point(60, 189);
            this.dedo1.Name = "dedo1";
            this.dedo1.Size = new System.Drawing.Size(87, 101);
            this.dedo1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo1.TabIndex = 0;
            this.dedo1.TabStop = false;
            this.dedo1.Click += new System.EventHandler(this.dedo1_Click);
            this.dedo1.MouseLeave += new System.EventHandler(this.dedo1_MouseLeave);
            this.dedo1.MouseHover += new System.EventHandler(this.dedo1_MouseHover);
            // 
            // dedo2
            // 
            this.dedo2.BackColor = System.Drawing.Color.Transparent;
            this.dedo2.Image = ((System.Drawing.Image)(resources.GetObject("dedo2.Image")));
            this.dedo2.Location = new System.Drawing.Point(105, 132);
            this.dedo2.Name = "dedo2";
            this.dedo2.Size = new System.Drawing.Size(102, 124);
            this.dedo2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo2.TabIndex = 1;
            this.dedo2.TabStop = false;
            this.dedo2.Click += new System.EventHandler(this.dedo2_Click);
            this.dedo2.MouseLeave += new System.EventHandler(this.dedo2_MouseLeave);
            this.dedo2.MouseHover += new System.EventHandler(this.dedo2_MouseHover);
            // 
            // dedo3
            // 
            this.dedo3.BackColor = System.Drawing.Color.Transparent;
            this.dedo3.Image = ((System.Drawing.Image)(resources.GetObject("dedo3.Image")));
            this.dedo3.Location = new System.Drawing.Point(177, 119);
            this.dedo3.Name = "dedo3";
            this.dedo3.Size = new System.Drawing.Size(102, 124);
            this.dedo3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo3.TabIndex = 2;
            this.dedo3.TabStop = false;
            this.dedo3.Click += new System.EventHandler(this.dedo3_Click);
            this.dedo3.MouseLeave += new System.EventHandler(this.dedo3_MouseLeave);
            this.dedo3.MouseHover += new System.EventHandler(this.dedo3_MouseHover);
            // 
            // dedo4
            // 
            this.dedo4.BackColor = System.Drawing.Color.Transparent;
            this.dedo4.Image = ((System.Drawing.Image)(resources.GetObject("dedo4.Image")));
            this.dedo4.Location = new System.Drawing.Point(244, 133);
            this.dedo4.Name = "dedo4";
            this.dedo4.Size = new System.Drawing.Size(102, 124);
            this.dedo4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo4.TabIndex = 3;
            this.dedo4.TabStop = false;
            this.dedo4.Click += new System.EventHandler(this.dedo4_Click);
            this.dedo4.MouseLeave += new System.EventHandler(this.dedo4_MouseLeave);
            this.dedo4.MouseHover += new System.EventHandler(this.dedo4_MouseHover);
            // 
            // dedo5
            // 
            this.dedo5.BackColor = System.Drawing.Color.Transparent;
            this.dedo5.Image = ((System.Drawing.Image)(resources.GetObject("dedo5.Image")));
            this.dedo5.Location = new System.Drawing.Point(328, 249);
            this.dedo5.Name = "dedo5";
            this.dedo5.Size = new System.Drawing.Size(87, 101);
            this.dedo5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo5.TabIndex = 4;
            this.dedo5.TabStop = false;
            this.dedo5.Click += new System.EventHandler(this.dedo5_Click);
            this.dedo5.MouseLeave += new System.EventHandler(this.dedo5_MouseLeave);
            this.dedo5.MouseHover += new System.EventHandler(this.dedo5_MouseHover);
            // 
            // palma1
            // 
            this.palma1.BackColor = System.Drawing.Color.Transparent;
            this.palma1.Image = ((System.Drawing.Image)(resources.GetObject("palma1.Image")));
            this.palma1.Location = new System.Drawing.Point(139, 247);
            this.palma1.Name = "palma1";
            this.palma1.Size = new System.Drawing.Size(193, 180);
            this.palma1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.palma1.TabIndex = 5;
            this.palma1.TabStop = false;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(228, 24);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(475, 47);
            this.bunifuCustomLabel1.TabIndex = 6;
            this.bunifuCustomLabel1.Text = "Seleccione el dedo a registrar";
            // 
            // dedo10
            // 
            this.dedo10.BackColor = System.Drawing.Color.Transparent;
            this.dedo10.Image = ((System.Drawing.Image)(resources.GetObject("dedo10.Image")));
            this.dedo10.Location = new System.Drawing.Point(774, 193);
            this.dedo10.Name = "dedo10";
            this.dedo10.Size = new System.Drawing.Size(87, 101);
            this.dedo10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo10.TabIndex = 7;
            this.dedo10.TabStop = false;
            this.dedo10.Click += new System.EventHandler(this.dedo10_Click);
            this.dedo10.MouseLeave += new System.EventHandler(this.dedo10_MouseLeave);
            this.dedo10.MouseHover += new System.EventHandler(this.dedo10_MouseHover);
            // 
            // dedo9
            // 
            this.dedo9.BackColor = System.Drawing.Color.Transparent;
            this.dedo9.Image = ((System.Drawing.Image)(resources.GetObject("dedo9.Image")));
            this.dedo9.Location = new System.Drawing.Point(709, 134);
            this.dedo9.Name = "dedo9";
            this.dedo9.Size = new System.Drawing.Size(102, 124);
            this.dedo9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo9.TabIndex = 8;
            this.dedo9.TabStop = false;
            this.dedo9.Click += new System.EventHandler(this.dedo9_Click);
            this.dedo9.MouseLeave += new System.EventHandler(this.dedo9_MouseLeave);
            this.dedo9.MouseHover += new System.EventHandler(this.dedo9_MouseHover);
            // 
            // dedo6
            // 
            this.dedo6.BackColor = System.Drawing.Color.Transparent;
            this.dedo6.Image = ((System.Drawing.Image)(resources.GetObject("dedo6.Image")));
            this.dedo6.Location = new System.Drawing.Point(509, 250);
            this.dedo6.Name = "dedo6";
            this.dedo6.Size = new System.Drawing.Size(87, 101);
            this.dedo6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo6.TabIndex = 11;
            this.dedo6.TabStop = false;
            this.dedo6.Click += new System.EventHandler(this.dedo6_Click);
            this.dedo6.MouseLeave += new System.EventHandler(this.dedo6_MouseLeave);
            this.dedo6.MouseHover += new System.EventHandler(this.dedo6_MouseHover);
            // 
            // dedo8
            // 
            this.dedo8.BackColor = System.Drawing.Color.Transparent;
            this.dedo8.Image = ((System.Drawing.Image)(resources.GetObject("dedo8.Image")));
            this.dedo8.Location = new System.Drawing.Point(637, 121);
            this.dedo8.Name = "dedo8";
            this.dedo8.Size = new System.Drawing.Size(102, 124);
            this.dedo8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo8.TabIndex = 9;
            this.dedo8.TabStop = false;
            this.dedo8.Click += new System.EventHandler(this.dedo8_Click);
            this.dedo8.MouseLeave += new System.EventHandler(this.dedo8_MouseLeave);
            this.dedo8.MouseHover += new System.EventHandler(this.dedo8_MouseHover);
            // 
            // dedo7
            // 
            this.dedo7.BackColor = System.Drawing.Color.Transparent;
            this.dedo7.Image = ((System.Drawing.Image)(resources.GetObject("dedo7.Image")));
            this.dedo7.Location = new System.Drawing.Point(569, 135);
            this.dedo7.Name = "dedo7";
            this.dedo7.Size = new System.Drawing.Size(102, 124);
            this.dedo7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dedo7.TabIndex = 10;
            this.dedo7.TabStop = false;
            this.dedo7.Click += new System.EventHandler(this.dedo7_Click);
            this.dedo7.MouseLeave += new System.EventHandler(this.dedo7_MouseLeave);
            this.dedo7.MouseHover += new System.EventHandler(this.dedo7_MouseHover);
            // 
            // palma2
            // 
            this.palma2.BackColor = System.Drawing.Color.Transparent;
            this.palma2.Image = ((System.Drawing.Image)(resources.GetObject("palma2.Image")));
            this.palma2.Location = new System.Drawing.Point(591, 249);
            this.palma2.Name = "palma2";
            this.palma2.Size = new System.Drawing.Size(193, 180);
            this.palma2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.palma2.TabIndex = 12;
            this.palma2.TabStop = false;
            // 
            // SelectDedo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(939, 475);
            this.Controls.Add(this.dedo10);
            this.Controls.Add(this.dedo9);
            this.Controls.Add(this.dedo6);
            this.Controls.Add(this.dedo8);
            this.Controls.Add(this.dedo7);
            this.Controls.Add(this.palma2);
            this.Controls.Add(this.bunifuCustomLabel1);
            this.Controls.Add(this.dedo1);
            this.Controls.Add(this.dedo2);
            this.Controls.Add(this.dedo5);
            this.Controls.Add(this.dedo3);
            this.Controls.Add(this.dedo4);
            this.Controls.Add(this.palma1);
            this.Name = "SelectDedo";
            this.Style = MetroFramework.MetroColorStyle.Orange;
            ((System.ComponentModel.ISupportInitialize)(this.dedo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palma1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dedo7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.palma2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox dedo1;
        private System.Windows.Forms.PictureBox dedo2;
        private System.Windows.Forms.PictureBox dedo3;
        private System.Windows.Forms.PictureBox dedo4;
        private System.Windows.Forms.PictureBox dedo5;
        private System.Windows.Forms.PictureBox palma1;
        private ns1.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.PictureBox dedo10;
        private System.Windows.Forms.PictureBox dedo9;
        private System.Windows.Forms.PictureBox dedo6;
        private System.Windows.Forms.PictureBox dedo8;
        private System.Windows.Forms.PictureBox dedo7;
        private System.Windows.Forms.PictureBox palma2;
    }
}