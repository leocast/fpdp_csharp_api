﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Dynamic;
using System.IO;
using System.Drawing.Imaging;
using System.Net.Http;
using Library;

namespace ApiCSharp
{
    public partial class Registrar : MetroFramework.Forms.MetroForm
    {
        public dynamic Empleado;
        public dynamic template;
        private testController TC;

        public string base64Template;

        public Registrar(dynamic _empleado)
        {
            InitializeComponent();

            showLoader();

            this.Size = new Size(901, 312);

            this.Empleado = _empleado;

            TC = new testController();

            this.crearTemplates();

        }

        private void Registrar_Load(object sender, EventArgs e)
        {
            lblNombre.Text = $"{Empleado.nombre} {Empleado.apellidopat} {Empleado.apellidomat}";
            lblCodigo.Text = Empleado.codigo;
            lblPuesto.Text = Empleado.puesto;
        }

        public void setImagenHuella(Bitmap imagen, int dedo)
        {

            if (dedo == 1)
            {
                lblSinHuella.Visible = false;
                this.pbxHuella.Image = new Bitmap(imagen, this.pbxHuella.Size);
                this.pbxHuella.Location = new Point(482, 47);
            }
            else
            {
                lblSinHuella2.Visible = false;
                this.pbxHuella2.Image = new Bitmap(imagen, this.pbxHuella2.Size);
                //this.pbxHuella.Location = new Point(482, 47)
            }
        }

        private void btnRegistrarHuella_Click(object sender, EventArgs e)
        {
            EnrollmentApp enrollment = new EnrollmentApp(this, template.dedo1);
            enrollment.Show();
        }

        private void btnRegistrarHuella2_Click(object sender, EventArgs e)
        {
            EnrollmentApp enrollment = new EnrollmentApp(this, template.dedo2);
            enrollment.Show();
        }

        private async void crearTemplates()
        {
            template = await TC.getTemplates(Empleado.codigo);

            verificarDedosRegistrados();
        }

        private void verificarDedosRegistrados() {

            if (template.dedo1.registrado)
            {
                pbxHuella.SizeMode = PictureBoxSizeMode.StretchImage;
                pbxHuella.ImageLocation = @"images\registrado2.png";
                lblSinHuella.Text = "Registrado";
                lblSinHuella.Location = new Point(504, 188);
            }

            if (template.dedo2.registrado)
            {
                pbxHuella2.SizeMode = PictureBoxSizeMode.StretchImage;
                pbxHuella2.ImageLocation = @"images\registrado2.png";
                lblSinHuella2.Text = "Registrado";
                lblSinHuella2.Location = new Point(720, 188);
            }

            hideLoader();

        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            showLoader("Guardando...");

            var response = this.guardarTemplateDB();

            MessageForm MF = new MessageForm("Se han guardado los cambios");
            MF.ShowDialog();
            this.Close();
        }

        private async Task<dynamic> guardarTemplateDB() {

            return await TC.guardarEmpleado(Empleado.codigo, template);
        }

        private void showLoader(string message = "") {

            if (message != null)
                lblLoader.Text = message;

            loader.Location = new Point(19, 30);
        }

        private void hideLoader() {
            loader.Location = new Point(19, 323);
        }
     
    }
}
