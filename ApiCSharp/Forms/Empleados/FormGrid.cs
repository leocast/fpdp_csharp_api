﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Dynamic;
using Library;

namespace ApiCSharp
{
    public partial class FormGrid : MetroFramework.Forms.MetroForm
    {
        testController TC;
        List<dynamic> Empleados;

        public FormGrid()
        {
            InitializeComponent();
        }

        private void Form_Load(object sender, EventArgs e)
        {
            this.Size = new Size(837, 614);
            llenarDatagrid();
            txtLogger.Text = Session.usuario.nombre;
        }

        private async void llenarDatagrid(string search = "")
        {
            showLoader();

            TC = new testController();

            if (search != "")
                Empleados = await TC.getEmpleados(search);
            else
                Empleados = await TC.getEmpleados();

            while (!this.IsHandleCreated)
                System.Threading.Thread.Sleep(100);

            this.Invoke((MethodInvoker)delegate {

                dtsEmp.Rows.Clear();

                foreach (dynamic item in Empleados)
                {
                    dtsEmp.Rows.Add(item.codigo, item.nombre, item.apellidopat, item.apellidomat, item.puesto);
                }
            });

            hideLoader();

        }

        private void showLoader() {
            loader.Location = new Point(23, 92);
        }

        private void hideLoader() {
            while (!this.IsHandleCreated)
                System.Threading.Thread.Sleep(100);

            this.Invoke((MethodInvoker)delegate {
                loader.Location = new Point(845, 92);
            });
        }

        private void Form_FormClosed(object sender, FormClosedEventArgs e)
        {
            Application.Exit();
        }

        private void txtBuscar_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                llenarDatagrid(txtBuscar.Text);
        }

        private void btnBorrar_Click(object sender, EventArgs e)
        {
            if (txtBuscar.Text != "") {
                txtBuscar.Text = "";
                llenarDatagrid();
            }
        }

        private void dtsEmp_DoubleClick(object sender, EventArgs e)
        {
            DataGridViewCellCollection cells = dtsEmp.SelectedRows[0].Cells;

            dynamic Empleado = new ExpandoObject();

            Empleado.codigo       = cells[0].Value.ToString();
            Empleado.nombre       = cells[1].Value.ToString();
            Empleado.apellidopat  = cells[2].Value.ToString();
            Empleado.apellidomat  = cells[3].Value.ToString();
            Empleado.puesto       = cells[4].Value.ToString();

            Registrar reg = new Registrar(Empleado);
            reg.ShowDialog();
        }

        private void bunifuThinButton21_Click(object sender, EventArgs e)
        {
            Session.usuario = null;
            Login log = new Login();
            log.Show();
            this.Hide();
        }
    }
}
