﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ApiCSharp
{
    public partial class SelectDedo : MetroFramework.Forms.MetroForm
    {
        dynamic dedo;
        Registrar registrar;

        public SelectDedo(Registrar _registrar, dynamic _dedo)
        {
            InitializeComponent();
            registrar = _registrar;
            dedo = _dedo;
        }

        private void callEnrollment() {
            EnrollmentApp enrollment = new EnrollmentApp(registrar, dedo);
            enrollment.Show();
            this.Hide();
        }

        #region ChangeColor
        private void dedo1_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo1.ImageLocation = @"images\ManoIzq\1n.png";
        }

        private void dedo1_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo1.ImageLocation = @"images\ManoIzq\1.png";
        }

        private void dedo2_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo2.ImageLocation = @"images\ManoIzq\2n.png";
        }

        private void dedo2_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo2.ImageLocation = @"images\ManoIzq\2.png";
        }

        private void dedo3_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo3.ImageLocation = @"images\ManoIzq\3n.png";
        }

        private void dedo3_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo3.ImageLocation = @"images\ManoIzq\3.png";
        }

        private void dedo4_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo4.ImageLocation = @"images\ManoIzq\4n.png";
        }

        private void dedo4_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo4.ImageLocation = @"images\ManoIzq\4.png";
        }

        private void dedo5_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo5.ImageLocation = @"images\ManoDer\6n.png";
        }

        private void dedo5_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo5.ImageLocation = @"images\ManoDer\6.png";
        }

        private void dedo6_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo6.ImageLocation = @"images\ManoIzq\5n.png";
        }

        private void dedo6_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo6.ImageLocation = @"images\ManoIzq\5.png";
        }

        private void dedo7_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo7.ImageLocation = @"images\ManoDer\7n.png";
        }

        private void dedo7_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo7.ImageLocation = @"images\ManoDer\7.png";
        }

        private void dedo8_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo8.ImageLocation = @"images\ManoDer\8n.png";
        }

        private void dedo8_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo8.ImageLocation = @"images\ManoDer\8.png";
        }

        private void dedo9_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo9.ImageLocation = @"images\ManoDer\9n.png";
        }

        private void dedo9_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo9.ImageLocation = @"images\ManoDer\9.png";
        }

        private void dedo10_MouseHover(object sender, EventArgs e)
        {
            Cursor = Cursors.Hand;
            dedo10.ImageLocation = @"images\ManoDer\10n.png";
        }

        private void dedo10_MouseLeave(object sender, EventArgs e)
        {
            Cursor = Cursors.Default;
            dedo10.ImageLocation = @"images\ManoDer\10.png";
        }

        #endregion

        #region Clicks
        private void dedo1_Click(object sender, EventArgs e)
        {
            dedo.numero = 1;
            callEnrollment();
        }

        private void dedo2_Click(object sender, EventArgs e)
        {
            dedo.numero = 2;
            callEnrollment();
        }

        private void dedo3_Click(object sender, EventArgs e)
        {
            dedo.numero = 3;
            callEnrollment();
        }

        private void dedo4_Click(object sender, EventArgs e)
        {
            dedo.numero = 4;
            callEnrollment();
        }

        private void dedo5_Click(object sender, EventArgs e)
        {
            dedo.numero = 5;
            callEnrollment();
        }

        private void dedo6_Click(object sender, EventArgs e)
        {
            dedo.numero = 6;
            callEnrollment();
        }

        private void dedo7_Click(object sender, EventArgs e)
        {
            dedo.numero = 7;
            callEnrollment();
        }

        private void dedo8_Click(object sender, EventArgs e)
        {
            dedo.numero = 8;
            callEnrollment();
        }

        private void dedo9_Click(object sender, EventArgs e)
        {
            dedo.numero = 9;
            callEnrollment();
        }

        private void dedo10_Click(object sender, EventArgs e)
        {
            dedo.numero = 10;
            callEnrollment();
        }
        #endregion
    }
}
