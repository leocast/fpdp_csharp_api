﻿namespace ApiCSharp
{
    partial class Registrar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Registrar));
            this.bunifuElipse1 = new ns1.BunifuElipse(this.components);
            this.lblCodigo = new ns1.BunifuCustomLabel();
            this.pbxHuella = new System.Windows.Forms.PictureBox();
            this.lblNombre = new ns1.BunifuCustomLabel();
            this.lblPuesto = new ns1.BunifuCustomLabel();
            this.btnRegistrarHuella = new ns1.BunifuThinButton2();
            this.lblSinHuella2 = new ns1.BunifuCustomLabel();
            this.pbxHuella2 = new System.Windows.Forms.PictureBox();
            this.btnGuardar = new ns1.BunifuThinButton2();
            this.btnRegistrarHuella2 = new ns1.BunifuThinButton2();
            this.lblSinHuella = new ns1.BunifuCustomLabel();
            this.lblLoader = new ns1.BunifuCustomLabel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.loader = new MetroFramework.Controls.MetroPanel();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHuella)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHuella2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.loader.SuspendLayout();
            this.SuspendLayout();
            // 
            // bunifuElipse1
            // 
            this.bunifuElipse1.ElipseRadius = 10;
            this.bunifuElipse1.TargetControl = this;
            // 
            // lblCodigo
            // 
            this.lblCodigo.AutoSize = true;
            this.lblCodigo.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodigo.Location = new System.Drawing.Point(24, 133);
            this.lblCodigo.Name = "lblCodigo";
            this.lblCodigo.Size = new System.Drawing.Size(109, 37);
            this.lblCodigo.TabIndex = 1;
            this.lblCodigo.Text = "LCASLO";
            // 
            // pbxHuella
            // 
            this.pbxHuella.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.pbxHuella.Location = new System.Drawing.Point(489, 47);
            this.pbxHuella.Name = "pbxHuella";
            this.pbxHuella.Size = new System.Drawing.Size(174, 189);
            this.pbxHuella.TabIndex = 2;
            this.pbxHuella.TabStop = false;
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNombre.Location = new System.Drawing.Point(23, 69);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(428, 47);
            this.lblNombre.TabIndex = 3;
            this.lblNombre.Text = "Leonel Castellanos Alvarez";
            // 
            // lblPuesto
            // 
            this.lblPuesto.AutoSize = true;
            this.lblPuesto.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPuesto.Location = new System.Drawing.Point(24, 182);
            this.lblPuesto.Name = "lblPuesto";
            this.lblPuesto.Size = new System.Drawing.Size(178, 37);
            this.lblPuesto.TabIndex = 4;
            this.lblPuesto.Text = "Desarrollador";
            // 
            // btnRegistrarHuella
            // 
            this.btnRegistrarHuella.ActiveBorderThickness = 1;
            this.btnRegistrarHuella.ActiveCornerRadius = 20;
            this.btnRegistrarHuella.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella.ActiveForecolor = System.Drawing.Color.White;
            this.btnRegistrarHuella.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnRegistrarHuella.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRegistrarHuella.BackgroundImage")));
            this.btnRegistrarHuella.ButtonText = "Registrar";
            this.btnRegistrarHuella.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistrarHuella.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarHuella.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella.IdleBorderThickness = 1;
            this.btnRegistrarHuella.IdleCornerRadius = 20;
            this.btnRegistrarHuella.IdleFillColor = System.Drawing.Color.White;
            this.btnRegistrarHuella.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella.Location = new System.Drawing.Point(502, 243);
            this.btnRegistrarHuella.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRegistrarHuella.Name = "btnRegistrarHuella";
            this.btnRegistrarHuella.Size = new System.Drawing.Size(140, 36);
            this.btnRegistrarHuella.TabIndex = 16;
            this.btnRegistrarHuella.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnRegistrarHuella.Click += new System.EventHandler(this.btnRegistrarHuella_Click);
            // 
            // lblSinHuella2
            // 
            this.lblSinHuella2.AutoSize = true;
            this.lblSinHuella2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.lblSinHuella2.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSinHuella2.Location = new System.Drawing.Point(721, 121);
            this.lblSinHuella2.Name = "lblSinHuella2";
            this.lblSinHuella2.Size = new System.Drawing.Size(132, 37);
            this.lblSinHuella2.TabIndex = 19;
            this.lblSinHuella2.Text = "Sin huella";
            // 
            // pbxHuella2
            // 
            this.pbxHuella2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.pbxHuella2.Location = new System.Drawing.Point(700, 47);
            this.pbxHuella2.Name = "pbxHuella2";
            this.pbxHuella2.Size = new System.Drawing.Size(174, 189);
            this.pbxHuella2.TabIndex = 18;
            this.pbxHuella2.TabStop = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.ActiveBorderThickness = 1;
            this.btnGuardar.ActiveCornerRadius = 20;
            this.btnGuardar.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnGuardar.ActiveForecolor = System.Drawing.Color.White;
            this.btnGuardar.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnGuardar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnGuardar.BackgroundImage")));
            this.btnGuardar.ButtonText = "Guardar";
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGuardar.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnGuardar.IdleBorderThickness = 1;
            this.btnGuardar.IdleCornerRadius = 20;
            this.btnGuardar.IdleFillColor = System.Drawing.Color.White;
            this.btnGuardar.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnGuardar.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnGuardar.Location = new System.Drawing.Point(31, 243);
            this.btnGuardar.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(420, 36);
            this.btnGuardar.TabIndex = 21;
            this.btnGuardar.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // btnRegistrarHuella2
            // 
            this.btnRegistrarHuella2.ActiveBorderThickness = 1;
            this.btnRegistrarHuella2.ActiveCornerRadius = 20;
            this.btnRegistrarHuella2.ActiveFillColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella2.ActiveForecolor = System.Drawing.Color.White;
            this.btnRegistrarHuella2.ActiveLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnRegistrarHuella2.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btnRegistrarHuella2.BackgroundImage")));
            this.btnRegistrarHuella2.ButtonText = "Registrar";
            this.btnRegistrarHuella2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRegistrarHuella2.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnRegistrarHuella2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella2.IdleBorderThickness = 1;
            this.btnRegistrarHuella2.IdleCornerRadius = 20;
            this.btnRegistrarHuella2.IdleFillColor = System.Drawing.Color.White;
            this.btnRegistrarHuella2.IdleForecolor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella2.IdleLineColor = System.Drawing.Color.FromArgb(((int)(((byte)(243)))), ((int)(((byte)(119)))), ((int)(((byte)(53)))));
            this.btnRegistrarHuella2.Location = new System.Drawing.Point(717, 243);
            this.btnRegistrarHuella2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRegistrarHuella2.Name = "btnRegistrarHuella2";
            this.btnRegistrarHuella2.Size = new System.Drawing.Size(140, 36);
            this.btnRegistrarHuella2.TabIndex = 22;
            this.btnRegistrarHuella2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnRegistrarHuella2.Click += new System.EventHandler(this.btnRegistrarHuella2_Click);
            // 
            // lblSinHuella
            // 
            this.lblSinHuella.AutoSize = true;
            this.lblSinHuella.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(229)))), ((int)(((byte)(229)))), ((int)(((byte)(229)))));
            this.lblSinHuella.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSinHuella.Location = new System.Drawing.Point(510, 121);
            this.lblSinHuella.Name = "lblSinHuella";
            this.lblSinHuella.Size = new System.Drawing.Size(132, 37);
            this.lblSinHuella.TabIndex = 14;
            this.lblSinHuella.Text = "Sin huella";
            // 
            // lblLoader
            // 
            this.lblLoader.AutoSize = true;
            this.lblLoader.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLoader.Location = new System.Drawing.Point(292, 171);
            this.lblLoader.Name = "lblLoader";
            this.lblLoader.Size = new System.Drawing.Size(248, 31);
            this.lblLoader.TabIndex = 3;
            this.lblLoader.Text = "Verificando datos...";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(283, 28);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(267, 174);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 4;
            this.pictureBox3.TabStop = false;
            // 
            // loader
            // 
            this.loader.Controls.Add(this.lblLoader);
            this.loader.Controls.Add(this.pictureBox3);
            this.loader.HorizontalScrollbarBarColor = true;
            this.loader.HorizontalScrollbarHighlightOnWheel = false;
            this.loader.HorizontalScrollbarSize = 10;
            this.loader.Location = new System.Drawing.Point(19, 323);
            this.loader.Name = "loader";
            this.loader.Size = new System.Drawing.Size(855, 299);
            this.loader.TabIndex = 23;
            this.loader.VerticalScrollbarBarColor = true;
            this.loader.VerticalScrollbarHighlightOnWheel = false;
            this.loader.VerticalScrollbarSize = 10;
            // 
            // Registrar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(901, 629);
            this.Controls.Add(this.loader);
            this.Controls.Add(this.btnRegistrarHuella2);
            this.Controls.Add(this.btnGuardar);
            this.Controls.Add(this.lblSinHuella2);
            this.Controls.Add(this.pbxHuella2);
            this.Controls.Add(this.btnRegistrarHuella);
            this.Controls.Add(this.lblSinHuella);
            this.Controls.Add(this.lblPuesto);
            this.Controls.Add(this.lblCodigo);
            this.Controls.Add(this.pbxHuella);
            this.Controls.Add(this.lblNombre);
            this.MaximizeBox = false;
            this.Name = "Registrar";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroFormShadowType.DropShadow;
            this.Style = MetroFramework.MetroColorStyle.Orange;
            this.Load += new System.EventHandler(this.Registrar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pbxHuella)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbxHuella2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.loader.ResumeLayout(false);
            this.loader.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private ns1.BunifuElipse bunifuElipse1;
        private System.Windows.Forms.PictureBox pbxHuella;
        private ns1.BunifuCustomLabel lblCodigo;
        private ns1.BunifuCustomLabel lblNombre;
        private ns1.BunifuCustomLabel lblPuesto;
        private ns1.BunifuThinButton2 btnRegistrarHuella;
        private ns1.BunifuThinButton2 btnRegistrarHuella2;
        private ns1.BunifuThinButton2 btnGuardar;
        private ns1.BunifuCustomLabel lblSinHuella2;
        private System.Windows.Forms.PictureBox pbxHuella2;
        private ns1.BunifuCustomLabel lblSinHuella;
        private MetroFramework.Controls.MetroPanel loader;
        private ns1.BunifuCustomLabel lblLoader;
        private System.Windows.Forms.PictureBox pictureBox3;
    }
}