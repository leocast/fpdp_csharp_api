﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Enrollment;

namespace ApiCSharp
{
    public partial class Capture : Form, DPFP.Capture.EventHandler
    {
        public Capture()
        {
            InitializeComponent();
            FP_Controller FP = new FP_Controller(this);
        }

        protected void makeLog(string message)
        {
            while (!this.IsHandleCreated)
                System.Threading.Thread.Sleep(100);

            this.Invoke((MethodInvoker)delegate{
                txtLog.AppendText(">> " + message + "\r\n");
                lblText.Text = message;
            });
           
        }

        protected async void verify(DPFP.Sample _sample)
        {
            Verification verification = new Verification(_sample);

            bool result = await verification.check();

            if (result)
            {
                MessageBox.Show("La huella es correcta");
            }
            else
            {
                MessageBox.Show("La huella no es correcta");
            }

        }

        #region Eventos:
        public void OnComplete(object Capture, string ReaderSerialNumber, DPFP.Sample Sample)
        {
            makeLog("El sample ha sido capturado");

            MessageBox.Show("Se iniciará la verificación...");

            this.verify(Sample);
        }

        public void OnFingerGone(object Capture, string ReaderSerialNumber)
        {
            makeLog("El dedo ha sido removido del lector");
        }

        public void OnFingerTouch(object Capture, string ReaderSerialNumber)
        {
            makeLog("El lector ha sido tocado");
        }

        public void OnReaderConnect(object Capture, string ReaderSerialNumber)
        {
            makeLog("El lector ha sido conectado");
        }

        public void OnReaderDisconnect(object Capture, string ReaderSerialNumber)
        {
            makeLog("El lector ha sido deconectado");
        }

        public void OnSampleQuality(object Capture, string ReaderSerialNumber, DPFP.Capture.CaptureFeedback CaptureFeedback)
        {
            if (CaptureFeedback == DPFP.Capture.CaptureFeedback.Good)
                makeLog("La calidad de la huella es buena :D");
            else
                makeLog("La calidad de la huella es mala :(");
        }
        #endregion
    }
}
