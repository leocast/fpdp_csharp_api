﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Library;
using System.IO;
using System.Net.Http;

namespace ApiCSharp
{
    public partial class MainCapture : MetroFramework.Forms.MetroForm
    {
        testController TC;

        public MainCapture()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TC = new testController();
        }

        private async void btnGetDatos_Click(object sender, EventArgs e)
        {
            var response = await TC.getData("getEmpleado");

            txtText.Text = Helper.formatJson(response);
        }

        private async void btnSend_Click(object sender, EventArgs e)
        {
            byte[] fingerPrint = File.ReadAllBytes("test.fpt"); //Convertimos el archivo a Byte[]

            string fingerPrintB64 = Convert.ToBase64String(fingerPrint); //Pasames el arreglo a Base64

            MultipartFormDataContent data = new MultipartFormDataContent(); //Se crea el MultipartForm para poder cargar los datos

            data.Add(new StringContent(fingerPrintB64), "fingerPrint"); //Se añade la data, con el tipo requerido

            var response = await TC.sendData("csharp", data); //Se envia al API y se guarda la respuesta en "response"

            txtText.Text = Helper.formatJson(response); //Se imprime el resultado
        }

        private void btnMatch_Click(object sender, EventArgs e)
        {
            Capture form = new Capture();
            form.ShowDialog();
        }
    }
}
