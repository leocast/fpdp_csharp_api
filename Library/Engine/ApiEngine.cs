﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Library
{
    public static class ApiEngine
    {
        public static HttpClient ApiClient { get; set; }

        public static string url = "http://geniuscode.ddns.net:81/gtranem/api/";
        public static void InitClient()
        {
            ApiClient = ApiClient != null ? ApiClient : new HttpClient();
            ApiClient.DefaultRequestHeaders.Accept.Clear();
            ApiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }
    }
}
