﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using System.Dynamic;

namespace Library
{
    public class testController : Controller
    {
        public testController() {
            url = $"{ApiEngine.url}test/";
        }

        //MÉTODOS ESPECÍFICOS
        public async Task<DPFP.Template> getTemplate() {

            MultipartFormDataContent data = new MultipartFormDataContent(); //Se crea el MultipartForm para poder cargar los datos

            string codigoEmpleado = "LCASLO"; //El id del empleado obtenido

            data.Add(new StringContent(codigoEmpleado), "codigoEmpleado"); //Se añade la data, con el tipo requerido

            var response = await this.sendData("getTemplate", data); //Se envia al API y se guarda la respuesta en "response"

            byte[] templateBytes = Convert.FromBase64String(response); //Se transforma la Base64 a bytes[]

            MemoryStream ms = new MemoryStream(templateBytes); //Se encapsulan los datos en la memoria

            DPFP.Template template = new DPFP.Template(ms); //Se crea un fp template con los datos del stream

            return template;

        }

        public async Task<dynamic> login(string user, string pass)
        {
            MultipartFormDataContent data = new MultipartFormDataContent();

            data.Add(new StringContent(user), "usuario");
            data.Add(new StringContent(pass), "contrasena");

            var response = await this.sendData("login", data);

            dynamic usuario = JsonConvert.DeserializeObject(response);

            return usuario;
        }

        public async Task<List<dynamic>> getEmpleados(string busqueda = "") {

            var response = "";

            if (busqueda != "")
            {
                MultipartFormDataContent data = new MultipartFormDataContent();

                data.Add(new StringContent(busqueda), "buscar");

                response = await this.sendData("getEmpleados", data);
            }
            else {
                response = await this.getData("getEmpleados");
            }

            List<dynamic> empleados = JsonConvert.DeserializeObject<List<dynamic>>(response);

            return empleados;

        }

        public async Task<dynamic> guardarEmpleado(string codigoEmpleado, dynamic template) {

            MultipartFormDataContent data = new MultipartFormDataContent();

            data.Add(new StringContent(codigoEmpleado), "codigoEmpleado");
            data.Add(new StringContent(template.dedo1.template64), "dedo1");
            data.Add(new StringContent(template.dedo2.template64), "dedo2");

            var response = await this.sendData("guardarTemplateEmpleado", data);

            dynamic responseObject = JsonConvert.DeserializeObject<dynamic>(response);

            return responseObject;
        }

        public async Task<dynamic> getTemplates(string codigoEmpleado)
        {

            MultipartFormDataContent data = new MultipartFormDataContent();

            data.Add(new StringContent(codigoEmpleado), "codigoEmpleado");

            string response = await this.sendData("getTemplates", data);

            string[] templates64 = response.Split(new[] { "[separadorOficial]" }, StringSplitOptions.None);

            dynamic template = new ExpandoObject();
            template.dedo1 = new ExpandoObject();
            template.dedo2 = new ExpandoObject();

            template.dedo1.dedo = 1;
            template.dedo1.numero = 0;
            template.dedo1.template64 = templates64[0];
            template.dedo1.registrado = templates64[0] != "" ? true : false;

            template.dedo2.dedo = 2;
            template.dedo2.numero = 0;
            template.dedo2.template64 = templates64[1];
            template.dedo2.registrado = templates64[1] != "" ? true : false;

            return template;

        }
    }
}
