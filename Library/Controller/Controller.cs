﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;

namespace Library
{
    public class Controller
    {
        public string url;

        public Controller() {
            ApiEngine.InitClient();
        }

        public async Task<string> getData(string _method)
        {
            using (HttpResponseMessage response = await ApiEngine.ApiClient.GetAsync(url + _method))
            {
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    return content;
                }
                else
                {
                    Console.WriteLine("Error -> -> ->   ");
                    Console.WriteLine(response.ReasonPhrase);

                    string content = await response.Content.ReadAsStringAsync();
                    throw new Exception(content);
                }
            }
        }

        public async Task<string> sendData(string _method, MultipartFormDataContent _data)
        {
      
            var request = new HttpRequestMessage()
            {
                RequestUri = new Uri(url + _method),
                Method = HttpMethod.Post,
            };

            request.Content = _data;


            using (HttpResponseMessage response = await ApiEngine.ApiClient.SendAsync(request))
            {
                if (response.IsSuccessStatusCode)
                {
                    string content = await response.Content.ReadAsStringAsync();

                    return content;
                }
                else
                {
                    Console.WriteLine("Error -> -> ->   ");
                    Console.WriteLine(response.ReasonPhrase);

                    string content = await response.Content.ReadAsStringAsync();
                    throw new Exception(content);
                }
            }
        }
    }
}
